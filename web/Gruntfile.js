module.exports = function (grunt) {
    require("matchdep").filterDev("grunt-*").forEach(grunt.loadNpmTasks);
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        concat: {
            options: {
                separator: ';'
            },
            jquery: {
                src: [
                    'bower_components/jquery/dist/jquery.min.js',
                    'bower_components/bootstrap/dist/js/bootstrap.min.js'
                ],
                dest: 'build/tmp/<%= pkg.name %>.jq.concat.js'
            },
            site: {
                src: [
                    'build/custom/js/site.js'
                ],
                dest: 'build/tmp/<%= pkg.name %>.site.concat.js'
            }
        },
        less: {
            development: {
                options: {
                    paths: ['bower_components/bootstrap/less']
                },
                files: {
                    'build/tmp/main.css': 'build/custom/less/bootstrap-custom.less'
                }
            },
            production: {
                options: {
                    paths: ['bower_components/bootstrap/less']
                },
                files: {
                    'build/tmp/main.css': 'build/custom/less/bootstrap-custom.less'
                }
            }
        },
        cssmin: {
            development: {
                options: {
                    banner: ""
                },
                files: {
                    'public/assets/css/main.min.css': [
                        'build/tmp/main.css'
                    ],
                }
            },
            production: {
                options: {
                    banner: ""
                },
                files: {
                    'public/assets/css/main.min.css': [
                        'build/tmp/main.css'
                    ],
                }
            }
        },
        /*
         clean: [
         '<%= concat.jquery.dest %>',
         '<%= uglify.jquery.dest %>',
         '<%= concat.site.dest %>',
         '<%= uglify.site.dest %>',
         '<%= concat.admin.dest %>',
         '<%= uglify.admin.dest %>'
         ],
         */
        watch: {
            scripts: {
                files:[],
                tasks:[],
                options: {
                    spawn: false
                }
            }
        },
        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n',
                preserveComments: false,
                mangle: false,
                compress: true
            },
            jquery: {
                src: [
                    'build/tmp/<%= pkg.name %>.jq.concat.js'
                ],
                dest: 'public/assets/js/jquery.min.js'
            },
            site: {
                src: [
                    'build/tmp/<%= pkg.name %>.site.concat.js'
                ],
                dest: 'public/assets/js/<%= pkg.name %>.site.min.js'
            },
        },
        /* Copy font files: */
        copy: {
            bt_fonts: {
                files: [
                    // include files within path, all files in subdirectories will be copied to destination path
                    {
                        expand: true,
                        flatten: true,
                        src: ['bower_components/bootstrap/fonts/**'],
                        dest: 'public/assets/fonts/',
                        filter: 'isFile'
                    },
                ],
            },
        },
        clean: {
            concat: {
                src: [ '<%= concat.jquery.dest %>','<%= concat.site.dest %>' ]
            },
            uglify: {
                src: [ '<%= uglify.jquery.dest %>','<%= uglify.site.dest %>']
            },
            cssmin: {
                src: [ 'build/tmp/main.css' ]
            }
        }
    });
    // Default task.
    grunt.registerTask('default', ['clean:concat','clean:uglify','less', 'cssmin', 'concat', 'uglify','copy:bt_fonts','clean:concat','clean:cssmin']);
    grunt.registerTask('js', ['clean:concat','clean:uglify', 'concat', 'uglify','clean:concat']);
    grunt.registerTask('copy_fonts', ['copy:bt_fonts']);
};