<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
        Schema::create('messages', function($table)
        {
            $table->string('uid')->unique();
            $table->string('message');
            $table->string('status');
            $table->string('confirmed');
            $table->string('date_created');
            $table->string('date_expires');
            $table->string('ip_created');
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
        Schema::drop('messages');
	}

}
